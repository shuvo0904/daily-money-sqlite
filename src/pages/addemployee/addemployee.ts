import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SQLite, SQLiteObject  } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';

/**
 * Generated class for the AddemployeePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addemployee',
  templateUrl: 'addemployee.html',
})
export class AddemployeePage {
  data = {
    name:''
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, private sqlite: SQLite, public toast: Toast) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddemployeePage');
  }

  saveCategory(){
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
      
          db.executeSql('INSERT INTO category VALUES (NULL,?)', [
            this.data.name
          ])
            .then((res) =>{
              console.log('Executed SQL');
              this.toast.show('Category Insert Successfully', '5000', 'center').subscribe(
                toast => {
                  console.log(toast);
                  this.navCtrl.popToRoot();
                }
              );

            })
            .catch(e => {
              console.log(e)
              this.toast.show(e, '5000', 'center').subscribe(
                toast => {
                  console.log(toast);
                }
              );
            });
      
      
        })
        .catch(e => console.log(e));
  }

}
