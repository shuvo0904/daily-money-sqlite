import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddexpensesPage } from './addexpenses';

@NgModule({
  declarations: [
    AddexpensesPage,
  ],
  imports: [
    IonicPageModule.forChild(AddexpensesPage),
  ],
})
export class AddexpensesPageModule {}
