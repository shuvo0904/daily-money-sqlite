import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SQLite, SQLiteObject  } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';

/**
 * Generated class for the EditemployeePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editemployee',
  templateUrl: 'editemployee.html',
})
export class EditemployeePage {

  data = {
    id: 0,
    name:''
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, private sqlite: SQLite, public toast: Toast) {
    this.data.id = navParams.get('id');
    this.data.name = navParams.get('name');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditemployeePage');
  }

  updateCategory(){
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
      
          db.executeSql('UPDATE category SET name=? WHERE id=?', [
            this.data.id,
            this.data.name
          ])
            .then((res) =>{
              console.log('Executed SQL');
              this.toast.show('Category Insert Successfully', '5000', 'center').subscribe(
                toast => {
                  console.log(toast);
                  this.navCtrl.popToRoot();
                }
              );

            })
            .catch(e => {
              console.log(e)
              this.toast.show(e, '5000', 'center').subscribe(
                toast => {
                  console.log(toast);
                }
              );
            });
      
      
        })
        .catch(e => console.log(e));
  }

}
