import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditexpensesPage } from './editexpenses';

@NgModule({
  declarations: [
    EditexpensesPage,
  ],
  imports: [
    IonicPageModule.forChild(EditexpensesPage),
  ],
})
export class EditexpensesPageModule {}
