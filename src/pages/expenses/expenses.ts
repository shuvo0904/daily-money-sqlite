import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AddexpensesPage } from '../addexpenses/addexpenses';
import { EditexpensesPage } from '../editexpenses/editexpenses';

import { SQLite, SQLiteObject  } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
/**
 * Generated class for the ExpensesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-expenses',
  templateUrl: 'expenses.html',
})
export class ExpensesPage {

  data:any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private sqlite: SQLite, private toast:Toast) {
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExpensesPage');
    this.getData();
  }

  refresh(){
    this.getData();
  }

  getData(){
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
    
    
        db.executeSql('CREATE TABLE expenses(id int NOT NULL PRIMARY KEY, title TEXT, amount DECIMAL, category_id INT)', [])
          .then(() => {
            console.log('Executed SQL TABLE CREATE');
          })
          .catch(e => console.log(e));
    
        db.executeSql('SELECT * FROM expenses ORDER BY id DESC', [])
          .then((res) => {
            console.log('Executed SQL TABLE SELECT');

            for(var i=0; i < res.rows.length; i++){
              this.data.push({
                id: res.rows.item(i).id,
                title: res.rows.item(i).title,
                amount:res.rows.item(i).amount
              })
            }

          })
          .catch(e => console.log(e));
    
      })
      .catch(e => console.log(e));
  }

  addExpenses(){
    this.navCtrl.push(AddexpensesPage);
  }

  editExpenses(id, title, category_id, amount){
    this.navCtrl.push(EditexpensesPage,{
      id: id,
      title: title,
      category_id: category_id,
      amount: amount
    })
  }

  deleteExpenses(id){

    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
    
    
        db.executeSql('DELETE FROM expenses WHERE i=?', [id])
          .then(() => console.log('Executed SQL'))
          .catch(e => console.log(e));
    
    
      })
      .catch(e => console.log(e));

  }




}
