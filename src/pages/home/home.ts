import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AddemployeePage } from '../addemployee/addemployee';
import { EditemployeePage } from '../editemployee/editemployee';

import { SQLite, SQLiteObject  } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  data:any = [];
  constructor(public navCtrl: NavController, private sqlite: SQLite, public toast: Toast) {
     this.getData();
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad Home Page');
    this.getData();
  }

  refresh(){
    this.getData();
  }

  getData(){
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
    
    
        db.executeSql('CREATE TABLE category(id int NOT NULL PRIMARY KEY, name TEXT)', [])
          .then(() => {
            console.log('Executed SQL TABLE CREATE');
          })
          .catch(e => console.log(e));
    
        db.executeSql('SELECT * FROM category ORDER BY id DESC', [])
          .then((res) => {
            console.log('Executed SQL TABLE SELECT');

            for(var i=0; i < res.rows.length; i++){
              this.data.push({
                id: res.rows.item(i).id,
                name: res.rows.item(i).name
              })
            }

          })
          .catch(e => console.log(e));
    
      })
      .catch(e => console.log(e));
  }

  addCategory(){
    this.navCtrl.push(AddemployeePage);
  }

  editCategory(id, name){
    this.navCtrl.push(EditemployeePage,{
      id: id,
      name: name
    })
  }

  deleteCategory(id){

    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
    
    
        db.executeSql('DELETE FROM category WHERE i=?', [id])
          .then(() => console.log('Executed SQL'))
          .catch(e => console.log(e));
    
    
      })
      .catch(e => console.log(e));

  }


}
